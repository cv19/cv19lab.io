
[![pipeline status](https://gitlab.com/cv19/cv19.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/cv19/cv19.gitlab.io/-/commits/master)


---
# Covid 19 Dashboard Germany
This project provides a simple dashboard for Germany about the Covid-19 pandemic according to the insights provided in [this minute physics video on interpreting epidemic numbers](https://youtu.be/54XLXg4fYsc).


[[_TOC_]]

---
## Building locally

This project uses [yarn](https://yarnpkg.com), you'll need to install this globally before you can get started.

```bash
# get yarn
$ npm install -g yarn

# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

Your website should be available at [http://localhost:3000/]

---

Made with [Nuxt][nuxt] and [GitLab Pages][gitlab-pages].
Icons used from [Icons8][icons8].


[nuxt]: https://nuxtjs.org/
[gitlab-pages]: https://docs.gitlab.com/ee/user/project/pages/
[icons8]: https://icons8.com/

----

Forked from @samdbeckham
